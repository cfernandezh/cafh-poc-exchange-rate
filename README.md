# Exchange Rate Service - cafh-poc-exchange-rate #

The main functionality of this service is converting amounts of money between currencies. Other functionalities are provided to enable the maintain of the exchange rates used in the conversion process.

### Cases of currency conversion ##

* **Convert USD to PEN** (1USD = 3.85PEN)

  | Amount | From Currency | To Currency | Exchange Rate | Converted Amount |
    | -------- | ----------------- |------------- | -------- | --------- |
  | 1000 | USD | PEN | 3.85 | 3850.00 |

* **Convert PEN to USD** (1PEN = 0.26USD)

  | Amount | From Currency | To Currency | Exchange Rate | Converted Amount |
    | -------- | ----------------- |------------- | -------- | --------- |
  | 1000 | PEN | USD | 0.26 | 260.95 |

***

## Data Structure ##

#### **Table: exchange_rate** ####

| Attribute            | Type                | Description                                          | Sample                               |  Key      |
| --------             | -----------------   | -------------                                        | --------                             | --------- |
| exchange_rate_id     | text(UUID)          | Exchange rate Identifier                             | cde558bb-9ddc-4165-97b2-89fd350be515 | PK        |
| currency             | text(3)             | Name of the target currency                          | PEN                                  | -         |
| exchange_rate        | decimal(6,4)        | Conversion rate value                                | 3.85                                 | -         |
| base_currency        | texto(3)            | Base name of the main currency                       | USD                                  | -         |
| enabled              | boolean             | Flag for soft deleted of the exchange rate           | 1                                    | -         |
| created_user         | text(255)           | User which perform the creation of the record        | cfernandezh                          | -         |
| created_date         | datetime            | Date of the creation of the record                   | 2021-11-12 13:34:12.12321412         | -         |
| last_modified_user   | text(255)           | User which perform the last update of the record     | cfernandezh                          | -         |
| last_modified_date   | datetime            | Date of the last update of the record                | 2021-11-12 13:34:12.12321412         | -         |

***
## Application Architecture ##

### Diagram ###

![Image alt text](Application_Structure.png)

###### **Business Facade** ######
Layer to concentrate the business logic of the application. Facade Pattern is used to implement it. This layer define the following components:
* Input Interfaces to receive the data from RestAdapter.
* Output Interface to send data to RepositoryAdapter.
* Business Class to implement the Input Interface and their methods concentrate the logic.
* Domain Class to represent the business data used in the logic and exposed through the interfaces.

###### **Rest Adapter** ######
Layer to receive and response al petitions through REST Http Methods(GET, POST, PUT, DELETE, OPTIONS, PATCH). This layer defines the following components:
* Adapter Class to implement the endpoints exposed as API. This class references Output Interface defined in the business layer.
* Request Class to represent the incoming data through an endpoint.
* Response Class to represent the returned data through an endpoint.
* Parser Class to prepare the request class to the domain class and prepare the domain class to response class.

###### **Repository Adapter** ######
Layer to persist data to a data store Like H2 and to obtain data from a data store like H2. This layer defines the following components:
* Adapter Class to implement the interaction to the data store managing transactions. This class implements the Input Interface defined in the business layer.
* Entity Class to represent the data to store and contains part of the domain data.
* Parser Class to prepare the domain class to entity class and prepare the entity class to the domain class.

### Endpoints ###

* Conversion Money [POST] http://localhost:5005/exchange-rate/conversion

* Update Exchange Rate [POST] http://localhost:5005/exchange-rate/{exchangeRateId}

* List Exchange Rate By Currency [GET] http://localhost:5005/exchange-rate?currency=USD

### Configuration Properties ###

| Variable | Default Value | Description | Sample | Required |
| -------- | ----------------- |------------- | -------- | --------- |
| APP_NAME | cafh-poc-exchange-rate | Name of the application |  cafh-poc-exchange-rate | No |
| APP_ENVIRONMENT | dev | Environment to deploy the application |  dev, qa, uat y pro | No |
| APP_PORT | 8008 | Port for the application |  5005 | No |
| DATABASE_USERNAME | sa | Username to access the DB | sa| No |
| DATABASE_PASSWORD |  | Password to access the DB | | No |
| LOG_LEVEL | INFO | Level of the log | DEBUG | No |
| JWT_EXPIRATION_TIME | 300 | Expiration Time of the JWT in seconds | | No |
| ENCRYPT_KEY | Demo_RxJava!2021 | Key used in the encryption | | No |

### Run the application ###
* Make sure to be in the root directory
* Clean and build the project, run the command:
```
mvn clean install
```
* This will also create the docker image.
```
docker images
```
* **(OPTIONAL)** - You can use Dockerfile to generate docker image
```
docker build -t cafh.poc.exchange.rate/cafh.poc.exchange.rate:0.0.1-SNAPSHOT ./
```
* To build and run the application use docker-compose
```
docker-compose -f docker-compose/services.yml up -d
```
***