package cafh.poc.exchange.rate.business;

import cafh.poc.exchange.rate.business.domain.ConversionDomain;
import cafh.poc.exchange.rate.business.domain.ExchangeRateDomain;
import cafh.poc.exchange.rate.business.input.ExchangeRateRepositoryPort;
import cafh.poc.exchange.rate.business.output.ExchangeRateService;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.UUID;

import static io.reactivex.Single.just;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ExchangeRateServiceTest {

    @Mock
    private ExchangeRateRepositoryPort exchangeRateRepository;

    private ExchangeRateService exchangeRateService;

    @BeforeEach
    void setUp() {
        this.exchangeRateService = new ExchangeRateServiceImpl(exchangeRateRepository);
    }

    @Test
    void testConvertAmountFromCurrencyToCurrencyWhenIsSuccessful() {
        ExchangeRateDomain exchangeRate = ExchangeRateDomain.builder()
                .currency("USD")
                .baseCurrency("PEN")
                .exchangeRate(new BigDecimal("3.85").setScale(2, RoundingMode.HALF_UP))
                .build();
        ConversionDomain conversionDomain = ConversionDomain.builder()
                .amount(new BigDecimal("1000.00"))
                .originCurrency(exchangeRate.getCurrency())
                .destinationCurrency(exchangeRate.getBaseCurrency())
                .exchangeRate(exchangeRate.getExchangeRate())
                .convertedAmount(exchangeRate.getExchangeRate().multiply(new BigDecimal("1000.00")))
                .build();
        TestObserver<ConversionDomain> testObserver = new TestObserver<>();
        when(exchangeRateRepository.getExchangeRateByBaseAndCurrency(eq("USD"), eq("PEN")))
                .thenReturn(just(exchangeRate));
        exchangeRateService.convertCurrency(new BigDecimal("1000.00"), "USD", "PEN")
                .subscribe(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertNoErrors();
        testObserver.assertValue(conversionDomain);
    }

    @Test
    void throwExceptionWhenExchangeRateIsNotConfigure() {
        ExchangeRateDomain exchangeRate = ExchangeRateDomain.builder()
                .currency("USD")
                .baseCurrency("PEN")
                .build();
        TestObserver<ConversionDomain> testObserver = new TestObserver<>();
        when(exchangeRateRepository.getExchangeRateByBaseAndCurrency(eq("USD"), eq("PEN")))
                .thenReturn(just(exchangeRate));
        exchangeRateService.convertCurrency(new BigDecimal("1000.00"), "USD", "PEN")
                .subscribe(testObserver);
        testObserver.assertNotComplete();
    }


    @Test
    void testUpdateExchangeRateWhenIsSuccessful() {
        ExchangeRateDomain domain = ExchangeRateDomain.builder()
                .exchangeRateId(UUID.randomUUID().toString())
                .currency("USD")
                .baseCurrency("PEN")
                .exchangeRate(new BigDecimal("3.45").setScale(2, RoundingMode.HALF_UP))
                .build();
        TestObserver<ExchangeRateDomain> testObserver = new TestObserver<>();
        when(exchangeRateRepository.updateExchangeRate(eq(domain)))
                .thenReturn(just(domain));
        exchangeRateService.updateExchangeRate(domain)
                .subscribe(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertNoErrors();
        testObserver.assertValue(domain);
        verify(exchangeRateRepository, times(1)).updateExchangeRate(eq(domain));
    }

    @Test
    void testGetAllExchangeRatesByCurrency() {
        ExchangeRateDomain domain = ExchangeRateDomain.builder()
                .exchangeRateId(UUID.randomUUID().toString())
                .currency("PEN")
                .exchangeRate(new BigDecimal("3.85").setScale(2, RoundingMode.HALF_UP))
                .baseCurrency("USD")
                .build();
        when(exchangeRateRepository.findAllByCurrency(eq("USD"))).thenReturn(Observable.just(domain));
        TestObserver<ExchangeRateDomain> testObserver = new TestObserver<>();
        exchangeRateService.getRatesByCurrency("USD").subscribe(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertNoErrors();
        testObserver.assertValue(domain);
        verify(exchangeRateRepository, times(1)).findAllByCurrency(eq("USD"));
    }

}
