package cafh.poc.exchange.rate.repository;

import cafh.poc.exchange.rate.business.domain.ExchangeRateDomain;
import cafh.poc.exchange.rate.business.input.ExchangeRateRepositoryPort;
import cafh.poc.exchange.rate.exception.GlobalErrorInfoException;
import cafh.poc.exchange.rate.repository.Entity.ExchangeRateModel;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ExchangeRateRepositoryAdapterTest {

    private ExchangeRateRepositoryPort exchangeRateRepositoryAdapter;

    @Mock
    private ExchangeRateRepository exchangeRateRepository;

    @BeforeEach
    void setUp() {
        ExchangeRateRepositoryParser exchangeRateRepositoryParser = new ExchangeRateRepositoryParser();
        this.exchangeRateRepositoryAdapter = new ExchangeRateRepositoryAdapter(exchangeRateRepository,
                exchangeRateRepositoryParser);
    }

    @Test
    void testGetExchangeRateForOriginCurrencyAndDestinationCurrency() {
        TestObserver<ExchangeRateDomain> testObserver = new TestObserver<>();
        ExchangeRateModel model = ExchangeRateModel.builder()
                .exchangeRateId(UUID.randomUUID().toString())
                .baseCurrency("USD")
                .currency("PEN")
                .exchangeRate(new BigDecimal("3.85").setScale(2, RoundingMode.HALF_UP))
                .build();
        when(exchangeRateRepository.findByBaseCurrencyAndCurrencyAndEnabled(eq("USD"),
                eq("PEN"), eq(true)))
                .thenReturn(Single.just(model));
        exchangeRateRepositoryAdapter.getExchangeRateByBaseAndCurrency("USD", "PEN")
                .subscribe(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        verify(exchangeRateRepository, times(1))
                .findByBaseCurrencyAndCurrencyAndEnabled(eq("USD"), eq("PEN"), eq(true));
    }

    @Test
    void throwExceptionWhenExchangeRateDoesNotExists() {
        TestObserver<ExchangeRateDomain> testObserver = new TestObserver<>();
        when(exchangeRateRepository.findByBaseCurrencyAndCurrencyAndEnabled(eq("USD"), eq("NZD"), eq(true)))
                .thenReturn(Single.error(Exception::new));
        exchangeRateRepositoryAdapter.getExchangeRateByBaseAndCurrency("USD", "NZD")
                .subscribe(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
        testObserver.assertError(GlobalErrorInfoException.class);
    }

    @Test
    void testUpdateExchangeRateWhenIdExists() {
        TestObserver<ExchangeRateDomain> testObserver = new TestObserver<>();
        ExchangeRateDomain domain = ExchangeRateDomain.builder()
                .exchangeRateId(UUID.randomUUID().toString())
                .baseCurrency("USD")
                .currency("PEN")
                .exchangeRate(new BigDecimal("3.85").setScale(2, RoundingMode.HALF_UP))
                .build();
        ExchangeRateModel model = ExchangeRateModel.builder()
                .exchangeRateId(domain.getExchangeRateId())
                .baseCurrency(domain.getBaseCurrency())
                .currency(domain.getCurrency())
                .exchangeRate(domain.getExchangeRate())
                .build();
        when(exchangeRateRepository.findByExchangeRateIdAndEnabled(
                eq(domain.getExchangeRateId()), eq(true)))
                .thenReturn(Single.just(model));
        when(exchangeRateRepository.setFixedExchangeRateFor(
                eq(domain.getExchangeRate()), eq(domain.getExchangeRateId())))
                .thenReturn(Single.just(1));
        exchangeRateRepositoryAdapter.updateExchangeRate(domain)
                .subscribe(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        verify(exchangeRateRepository, times(1))
                .findByExchangeRateIdAndEnabled(
                        eq(domain.getExchangeRateId()), eq(true));
        verify(exchangeRateRepository, times(1))
                .setFixedExchangeRateFor(eq(domain.getExchangeRate()), eq(domain.getExchangeRateId()));
    }

    @Test
    void testFindAllExchangeRatesByCurrency() {
        ExchangeRateDomain domain = ExchangeRateDomain.builder()
                .exchangeRateId(UUID.randomUUID().toString())
                .baseCurrency("USD")
                .currency("PEN")
                .exchangeRate(new BigDecimal("3.85").setScale(2, RoundingMode.HALF_UP))
                .build();
        ExchangeRateModel model = ExchangeRateModel.builder()
                .exchangeRateId(domain.getExchangeRateId())
                .baseCurrency(domain.getBaseCurrency())
                .currency(domain.getCurrency())
                .exchangeRate(domain.getExchangeRate())
                .build();
        TestObserver<ExchangeRateDomain> testObserver = new TestObserver<>();
        when(exchangeRateRepository.findByBaseCurrencyAndEnabled(eq("USD"), eq(true)))
                .thenReturn(Observable.just(model));
        exchangeRateRepositoryAdapter.findAllByCurrency("USD").subscribe(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        verify(exchangeRateRepository, times(1))
                .findByBaseCurrencyAndEnabled(eq("USD"), eq(true));
    }

}
