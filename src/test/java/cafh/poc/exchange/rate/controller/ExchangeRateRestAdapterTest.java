package cafh.poc.exchange.rate.controller;

import cafh.poc.exchange.rate.business.domain.ConversionDomain;
import cafh.poc.exchange.rate.business.domain.ExchangeRateDomain;
import cafh.poc.exchange.rate.business.output.ExchangeRateService;
import cafh.poc.exchange.rate.controller.request.ExchangeRateConversionRequest;
import cafh.poc.exchange.rate.controller.request.ExchangeRateUpdateRequest;
import cafh.poc.exchange.rate.controller.response.ExchangeRateConversionResponse;
import cafh.poc.exchange.rate.controller.response.ExchangeRateResponse;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.UUID;

import static io.reactivex.Single.just;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ExchangeRateRestAdapterTest {

    private ExchangeRateRestAdapter exchangeRateRestAdapter;

    @Mock
    private ExchangeRateService exchangeRateService;

    @BeforeEach
    void setUp() {
        ExchangeRateRestParser exchangeRateRestParser = new ExchangeRateRestParser();
        this.exchangeRateRestAdapter = new ExchangeRateRestAdapter(exchangeRateService, exchangeRateRestParser);
    }

    @Test
    void testConvertAmountFromUsdToPenIsSuccessful() {
        ConversionDomain domain = ConversionDomain.builder().build();
        ExchangeRateConversionRequest request = ExchangeRateConversionRequest.builder()
                .amount(new BigDecimal(1000))
                .originCurrency("USD")
                .destinationCurrency("PEN")
                .build();
        ExchangeRateConversionResponse response = ExchangeRateConversionResponse.builder()
                .build();
        when(exchangeRateService.convertCurrency(any(BigDecimal.class), anyString(), anyString()))
                .thenReturn(just(domain));
        TestObserver<ExchangeRateConversionResponse> testObserver = new TestObserver<>();
        exchangeRateRestAdapter.convertAmount(request, "token").subscribe(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertValue(response);
        verify(exchangeRateService, times(1))
                .convertCurrency(any(BigDecimal.class), anyString(), anyString());
    }

    @Test
    void testUpdateExchangeRateWhenIsSuccessful() {
        String exchangeRateId = UUID.randomUUID().toString();
        ExchangeRateUpdateRequest request = ExchangeRateUpdateRequest.builder()
                .exchangeRate(new BigDecimal("3.85").setScale(2, RoundingMode.HALF_UP))
                .build();
        ExchangeRateDomain domain = ExchangeRateDomain.builder()
                .exchangeRateId(exchangeRateId)
                .exchangeRate(request.getExchangeRate())
                .build();
        ExchangeRateResponse response = ExchangeRateResponse.builder()
                .exchangeRateId(domain.getExchangeRateId())
                .exchangeRate(domain.getExchangeRate())
                .destinationCurrency(domain.getCurrency())
                .originCurrency(domain.getBaseCurrency())
                .build();
        TestObserver<ExchangeRateResponse> testObserver = new TestObserver<>();
        when(exchangeRateService.updateExchangeRate(any(ExchangeRateDomain.class)))
                .thenReturn(Single.just(domain));
        exchangeRateRestAdapter.updateExchangeRate(exchangeRateId, request, "token").subscribe(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertValue(response);
        verify(exchangeRateService, times(1))
                .updateExchangeRate(any(ExchangeRateDomain.class));
    }

    @Test
    void testGetAllExchangeRatesByCurrency() {
        ExchangeRateDomain domain = ExchangeRateDomain.builder().build();
        when(exchangeRateService.getRatesByCurrency(eq("USD")))
                .thenReturn(Observable.just(domain));
        TestObserver<ExchangeRateResponse> testObserver = new TestObserver<>();
        exchangeRateRestAdapter.getExchangeRatesByCurrency("USD", "token").subscribe(testObserver);
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        verify(exchangeRateService, times(1))
                .getRatesByCurrency(eq("USD"));
    }

}
