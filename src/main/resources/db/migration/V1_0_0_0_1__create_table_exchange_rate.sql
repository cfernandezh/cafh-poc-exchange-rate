CREATE TABLE exchange_rate (
    exchange_rate_id	  VARCHAR(36)                        NOT NULL COMMENT 'Exchange rate UUID',
    currency              VARCHAR(3)                         NOT NULL COMMENT 'Name of a currency: USD, EUR, GBP',
    exchange_rate         DECIMAL(6, 4)                      NOT NULL COMMENT 'Conversion rate value',
    base_currency         VARCHAR(3)                         NOT NULL COMMENT 'Base name of the main currency: PEN',
    enabled               BIT DEFAULT 1                      NOT NULL COMMENT 'Flag for soft deleted of the exchange rate: [0: eliminated, 1: active]',
    created_user    	  VARCHAR(255)                       NOT NULL COMMENT 'User which perform the creation of the record',
    created_date    	  DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT 'Date of the creation of the record',
    last_modified_user    VARCHAR(255)                       NULL     COMMENT 'User which perform the last update of the record',
    last_modified_date    DATETIME 			                 NULL     COMMENT 'Date of the last update of the record',
    PRIMARY KEY (exchange_rate_id)
);