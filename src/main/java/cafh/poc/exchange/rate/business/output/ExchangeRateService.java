package cafh.poc.exchange.rate.business.output;

import cafh.poc.exchange.rate.business.domain.ConversionDomain;
import cafh.poc.exchange.rate.business.domain.ExchangeRateDomain;
import io.reactivex.Observable;
import io.reactivex.Single;

import java.math.BigDecimal;

public interface ExchangeRateService {

    Single<ConversionDomain> convertCurrency(BigDecimal amount, String fromCurrency, String toCurrency);

    Single<ExchangeRateDomain> updateExchangeRate(ExchangeRateDomain exchangeRate);

    Observable<ExchangeRateDomain> getRatesByCurrency(String currency);
}
