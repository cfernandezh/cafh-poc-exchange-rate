package cafh.poc.exchange.rate.business.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ConversionDomain {

    private BigDecimal amount;

    private String originCurrency;

    private String destinationCurrency;

    private BigDecimal convertedAmount;

    private BigDecimal exchangeRate;

}
