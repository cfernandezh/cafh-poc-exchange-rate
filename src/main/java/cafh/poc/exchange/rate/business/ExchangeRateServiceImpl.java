package cafh.poc.exchange.rate.business;

import cafh.poc.exchange.rate.business.domain.ConversionDomain;
import cafh.poc.exchange.rate.business.domain.ExchangeRateDomain;
import cafh.poc.exchange.rate.business.input.ExchangeRateRepositoryPort;
import cafh.poc.exchange.rate.business.output.ExchangeRateService;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
@Slf4j
public class ExchangeRateServiceImpl implements ExchangeRateService {

    private final ExchangeRateRepositoryPort exchangeRateRepository;

    public ExchangeRateServiceImpl(ExchangeRateRepositoryPort exchangeRateRepository) {
        this.exchangeRateRepository = exchangeRateRepository;
    }

    @Override
    public Single<ConversionDomain> convertCurrency(BigDecimal amount, String fromCurrency, String toCurrency) {
        return exchangeRateRepository
                .getExchangeRateByBaseAndCurrency(fromCurrency, toCurrency)
                .subscribeOn(Schedulers.io())
                .doOnSuccess(exchangeRateDomain -> log.info("Exchange rate found for {} to {} with rate: {}",
                        fromCurrency, toCurrency, exchangeRateDomain.getExchangeRate()))
                .doOnError(error -> log.error("Exchange rate does not exists for {} to {}",
                        fromCurrency, toCurrency))
                .map(exchangeRateDomain -> {
                    BigDecimal rate = exchangeRateDomain.getExchangeRate().setScale(2, RoundingMode.HALF_UP);
                    return ConversionDomain.builder()
                            .amount(amount)
                            .originCurrency(fromCurrency)
                            .destinationCurrency(toCurrency)
                            .exchangeRate(rate)
                            .convertedAmount(rate.multiply(amount))
                            .build();
                });
    }

    @Override
    public Single<ExchangeRateDomain> updateExchangeRate(ExchangeRateDomain exchangeRate) {
        return exchangeRateRepository
                .updateExchangeRate(exchangeRate)
                .subscribeOn(Schedulers.io())
                .doOnSuccess(domain -> log.info("Exchange rate updated for {} to {} with rate: {}",
                        domain.getBaseCurrency(), domain.getCurrency(), domain.getExchangeRate()))
                .doOnError(error -> log.error("Exchange rate does not exists for id: {}",
                        exchangeRate.getExchangeRateId()));
    }

    @Override
    public Observable<ExchangeRateDomain> getRatesByCurrency(String currency) {
        return exchangeRateRepository.findAllByCurrency(currency)
                .subscribeOn(Schedulers.io());
    }

}
