package cafh.poc.exchange.rate.business.input;

import cafh.poc.exchange.rate.business.domain.ExchangeRateDomain;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface ExchangeRateRepositoryPort {

    Single<ExchangeRateDomain> getExchangeRateByBaseAndCurrency(String baseCurrency, String targetCurrency);

    Single<ExchangeRateDomain> updateExchangeRate(ExchangeRateDomain exchangeRate);

    Observable<ExchangeRateDomain> findAllByCurrency(String currency);
}
