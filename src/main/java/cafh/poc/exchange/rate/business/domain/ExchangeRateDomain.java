package cafh.poc.exchange.rate.business.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExchangeRateDomain {

    private String exchangeRateId;

    private String currency;

    private BigDecimal exchangeRate;

    private String baseCurrency;

}
