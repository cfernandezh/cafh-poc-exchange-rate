package cafh.poc.exchange.rate.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.NestedRuntimeException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.reactive.function.UnsupportedMediaTypeException;
import org.springframework.web.server.ServerWebInputException;
import org.springframework.web.server.UnsupportedMediaTypeStatusException;

@RestControllerAdvice
@Slf4j
public class GlobalErrorInfoHandler {

    @ExceptionHandler(value = GlobalErrorInfoException.class)
    public ResponseEntity<ResultErrorBody> errorHandler(GlobalErrorInfoException exception) {
        ErrorInfoInterface errorInfo = exception.getErrorInfo();
        ResultErrorBody result = new ResultErrorBody(errorInfo);
        return new ResponseEntity<>(result, null, errorInfo.getStatus());
    }

    @ExceptionHandler(value = WebExchangeBindException.class)
    public ResponseEntity<Object> errorHandler(WebExchangeBindException exception) {
        ResultErrorBody result = new ResultErrorBody(GlobalErrorInfoEnum.NOT_VALID);
        result.setMessage(exception.getMessage());
        return new ResponseEntity(result, null, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({UnsupportedMediaTypeStatusException.class, UnsupportedMediaTypeException.class})
    public ResponseEntity<Object> errorHandler(NestedRuntimeException exception) {
        ResultErrorBody result = new ResultErrorBody(GlobalErrorInfoEnum.NOT_SUPPORTED);
        result.setMessage(exception.getMessage());
        return new ResponseEntity(result, null, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @ExceptionHandler(value = ServerWebInputException.class)
    public ResponseEntity<Object> errorHandler(ServerWebInputException exception) {
        ResultErrorBody result = new ResultErrorBody(GlobalErrorInfoEnum.NOT_VALID);
        result.setMessage(exception.getMessage());
        return new ResponseEntity(result, null, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> errorHandler(Exception exception) {
        ResultErrorBody result = new ResultErrorBody(GlobalErrorInfoEnum.SERVER_ERROR);
        result.setMessage(exception.getMessage());
        return new ResponseEntity(result, null, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
