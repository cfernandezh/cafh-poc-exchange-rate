package cafh.poc.exchange.rate.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public enum GlobalErrorInfoEnum implements ErrorInfoInterface {

    NOT_VALID("-1", "request not valid", HttpStatus.BAD_REQUEST),
    NOT_SUPPORTED("-2", "media type not supported", HttpStatus.UNSUPPORTED_MEDIA_TYPE),
    SERVER_ERROR("-3", "internal server error", HttpStatus.INTERNAL_SERVER_ERROR),
    UNAUTHORIZED("-4", "unauthorized", HttpStatus.UNAUTHORIZED),
    FORBIDDEN("-5", "forbidden", HttpStatus.FORBIDDEN);


    private String code;

    private String message;

    private HttpStatus status;

}
