package cafh.poc.exchange.rate.exception;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
public class ResultErrorBody {

    private String code;

    private String message;

    private Object result;

    public ResultErrorBody(ErrorInfoInterface errorInfo) {
        this.code = errorInfo.getCode();
        this.message = errorInfo.getMessage();
        this.result = errorInfo;
    }

}
