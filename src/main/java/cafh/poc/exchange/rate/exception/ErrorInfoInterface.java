package cafh.poc.exchange.rate.exception;

import org.springframework.http.HttpStatus;

public interface ErrorInfoInterface {

    String getCode();

    String getMessage();

    HttpStatus getStatus();

}
