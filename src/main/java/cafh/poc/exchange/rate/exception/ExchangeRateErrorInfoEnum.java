package cafh.poc.exchange.rate.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public enum ExchangeRateErrorInfoEnum implements ErrorInfoInterface {

    EXCHANGE_RATE_NOT_FOUND("EXR001", "Exchange rate not found", HttpStatus.NOT_FOUND);

    private String code;

    private String message;

    private HttpStatus status;

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public HttpStatus getStatus() {
        return status;
    }

}