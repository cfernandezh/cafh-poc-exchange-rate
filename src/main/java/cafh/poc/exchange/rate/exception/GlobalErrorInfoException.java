package cafh.poc.exchange.rate.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GlobalErrorInfoException extends RuntimeException {

    private ErrorInfoInterface errorInfo;

}
