package cafh.poc.exchange.rate.controller.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExchangeRateResponse {

    private String exchangeRateId;

    private String originCurrency;

    private String destinationCurrency;

    private BigDecimal exchangeRate;

}
