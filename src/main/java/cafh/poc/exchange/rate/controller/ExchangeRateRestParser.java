package cafh.poc.exchange.rate.controller;

import cafh.poc.exchange.rate.business.domain.ConversionDomain;
import cafh.poc.exchange.rate.business.domain.ExchangeRateDomain;
import cafh.poc.exchange.rate.controller.request.ExchangeRateUpdateRequest;
import cafh.poc.exchange.rate.controller.response.ExchangeRateConversionResponse;
import cafh.poc.exchange.rate.controller.response.ExchangeRateResponse;
import org.springframework.stereotype.Component;

@Component
public class ExchangeRateRestParser {


    public ExchangeRateConversionResponse convertToResponse(ConversionDomain domain) {
        return ExchangeRateConversionResponse.builder()
                .amount(domain.getAmount())
                .originCurrency(domain.getOriginCurrency())
                .destinationCurrency(domain.getDestinationCurrency())
                .convertedAmount(domain.getConvertedAmount())
                .exchangeRate(domain.getExchangeRate())
                .build();
    }

    public ExchangeRateResponse convertToResponse(ExchangeRateDomain domain) {
        return ExchangeRateResponse.builder()
                .exchangeRateId(domain.getExchangeRateId())
                .destinationCurrency(domain.getCurrency())
                .originCurrency(domain.getBaseCurrency())
                .exchangeRate(domain.getExchangeRate())
                .build();
    }

    public ExchangeRateDomain convertToDomain(String exchangeRateId, ExchangeRateUpdateRequest request) {
        return ExchangeRateDomain.builder()
                .exchangeRateId(exchangeRateId)
                .exchangeRate(request.getExchangeRate())
                .build();
    }


}
