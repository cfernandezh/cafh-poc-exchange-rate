package cafh.poc.exchange.rate.controller;

import cafh.poc.exchange.rate.business.output.ExchangeRateService;
import cafh.poc.exchange.rate.controller.request.ExchangeRateConversionRequest;
import cafh.poc.exchange.rate.controller.request.ExchangeRateUpdateRequest;
import cafh.poc.exchange.rate.controller.response.ExchangeRateConversionResponse;
import cafh.poc.exchange.rate.controller.response.ExchangeRateResponse;
import cafh.poc.exchange.rate.security.config.Authorize;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/exchange-rate")
@Slf4j
public class ExchangeRateRestAdapter {

    private final ExchangeRateService exchangeRateService;

    private final ExchangeRateRestParser exchangeRateRestParser;

    public ExchangeRateRestAdapter(final ExchangeRateService exchangeRateService,
                                   final ExchangeRateRestParser exchangeRateRestParser) {
        this.exchangeRateService = exchangeRateService;
        this.exchangeRateRestParser = exchangeRateRestParser;
    }

    @PostMapping(value = "/conversion", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Authorize({"ROLE_USER"})
    public Single<ExchangeRateConversionResponse> convertAmount(
            @RequestBody @Validated final ExchangeRateConversionRequest request,
            @RequestHeader("Authorization") final String token) {
        return exchangeRateService.convertCurrency(request.getAmount(), request.getOriginCurrency(),
                        request.getDestinationCurrency())
                .subscribeOn(Schedulers.io())
                .map(exchangeRateRestParser::convertToResponse);
    }

    @PostMapping(value = "/{exchangeRateId}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Authorize({"ROLE_ADMIN"})
    public Single<ExchangeRateResponse> updateExchangeRate(
            @PathVariable("exchangeRateId") final String exchangeRateId,
            @RequestBody @Validated ExchangeRateUpdateRequest exchangeRateUpdateRequest,
            @RequestHeader("Authorization") final String token) {
        return exchangeRateService.updateExchangeRate(
                        exchangeRateRestParser.convertToDomain(exchangeRateId, exchangeRateUpdateRequest))
                .subscribeOn(Schedulers.io())
                .map(exchangeRateRestParser::convertToResponse);
    }

    @GetMapping(value = "", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Authorize({"ROLE_USER", "ROLE_ADMIN"})
    public Observable<ExchangeRateResponse> getExchangeRatesByCurrency(
            @RequestParam final String currency,
            @RequestHeader("Authorization") final String token) {
        return exchangeRateService.getRatesByCurrency(currency)
                .subscribeOn(Schedulers.io())
                .map(exchangeRateRestParser::convertToResponse);
    }

}
