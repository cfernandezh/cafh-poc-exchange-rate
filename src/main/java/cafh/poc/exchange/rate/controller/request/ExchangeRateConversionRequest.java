package cafh.poc.exchange.rate.controller.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExchangeRateConversionRequest {

    @NotNull
    private BigDecimal amount;

    @NotNull
    @Size(min = 3, max = 3)
    private String originCurrency;

    @NotNull
    @Size(min = 3, max = 3)
    private String destinationCurrency;

}
