package cafh.poc.exchange.rate.repository.Entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(value = "exchange_rate")
public class ExchangeRateModel {

    @Id
    private String exchangeRateId;

    @Column("currency")
    private String currency;

    @Column("base_currency")
    private String baseCurrency;

    @Column("exchange_rate")
    private BigDecimal exchangeRate;

    @Column("enabled")
    private Boolean enabled;

    @Column("created_user")
    private String createdUser;

    @Column("created_date")
    private LocalDateTime createdDate;

    @Column("last_modified_user")
    private String lastModifiedUser;

    @Column("last_modified_date")
    private LocalDateTime lastModifiedDate;

}
