package cafh.poc.exchange.rate.repository;

import cafh.poc.exchange.rate.business.domain.ExchangeRateDomain;
import cafh.poc.exchange.rate.business.input.ExchangeRateRepositoryPort;
import cafh.poc.exchange.rate.exception.ExchangeRateErrorInfoEnum;
import cafh.poc.exchange.rate.exception.GlobalErrorInfoException;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ExchangeRateRepositoryAdapter implements ExchangeRateRepositoryPort {

    private final ExchangeRateRepository exchangeRateRepository;

    private final ExchangeRateRepositoryParser exchangeRateRepositoryParser;

    public ExchangeRateRepositoryAdapter(final ExchangeRateRepository exchangeRateRepository,
                                         final ExchangeRateRepositoryParser exchangeRateRepositoryParser) {
        this.exchangeRateRepository = exchangeRateRepository;
        this.exchangeRateRepositoryParser = exchangeRateRepositoryParser;
    }

    @Override
    public Single<ExchangeRateDomain> getExchangeRateByBaseAndCurrency(String baseCurrency, String targetCurrency) {
        return exchangeRateRepository.findByBaseCurrencyAndCurrencyAndEnabled(baseCurrency, targetCurrency, true)
                .subscribeOn(Schedulers.io())
                .doOnSuccess(exchangeRate -> log.info("Exchange rate for {} to {} with rate: {}",
                        baseCurrency, targetCurrency, exchangeRate.getExchangeRate()))
                .doOnError(error -> log.error("Exchange rate does not exists for {} to {}",
                        baseCurrency, targetCurrency))
                .onErrorResumeNext(error ->
                        Single.error(new GlobalErrorInfoException(ExchangeRateErrorInfoEnum.EXCHANGE_RATE_NOT_FOUND)))
                .map(exchangeRateRepositoryParser::convertToDomain);
    }

    @Override
    public Single<ExchangeRateDomain> updateExchangeRate(ExchangeRateDomain exchangeRate) {
        return exchangeRateRepository.findByExchangeRateIdAndEnabled(
                        exchangeRate.getExchangeRateId(), true)
                .subscribeOn(Schedulers.io())
                .doOnSuccess(model -> log.info("Exchange rate for {} to {} with rate: {}",
                        model.getBaseCurrency(), model.getCurrency(), model.getExchangeRate()))
                .doOnError(error -> log.error("Exchange rate does not exists for {} to {}",
                        exchangeRate.getBaseCurrency(), exchangeRate.getCurrency()))
                .onErrorResumeNext(error ->
                        Single.error(new GlobalErrorInfoException(ExchangeRateErrorInfoEnum.EXCHANGE_RATE_NOT_FOUND)))
                .zipWith(exchangeRateRepository.setFixedExchangeRateFor(exchangeRate.getExchangeRate(),
                                exchangeRate.getExchangeRateId()),
                        (model, countUpdated) -> {
                            model.setExchangeRate(exchangeRate.getExchangeRate());
                            return model;
                        })
                .map(exchangeRateRepositoryParser::convertToDomain);

    }

    @Override
    public Observable<ExchangeRateDomain> findAllByCurrency(String currency) {
        return exchangeRateRepository.findByBaseCurrencyAndEnabled(currency, true)
                .subscribeOn(Schedulers.io())
                .doOnError(error -> log.error("Error getting Exchange rates for {}", currency))
                .map(exchangeRateRepositoryParser::convertToDomain);
    }
}
