package cafh.poc.exchange.rate.repository;

import cafh.poc.exchange.rate.repository.Entity.ExchangeRateModel;
import io.reactivex.Observable;
import io.reactivex.Single;
import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public interface ExchangeRateRepository extends R2dbcRepository<ExchangeRateModel, String> {

    Single<ExchangeRateModel> findByBaseCurrencyAndCurrencyAndEnabled(String baseCurrency,
                                                                      String currency,
                                                                      boolean enabled);

    Single<ExchangeRateModel> findByExchangeRateIdAndEnabled(String exchangeRateId,
                                                             boolean enabled);

    @Modifying
    @Query("UPDATE exchange_rate SET exchange_rate = :exchangeRate WHERE exchange_rate_id = :exchangeRateId " +
            "AND enabled IS TRUE")
    Single<Integer> setFixedExchangeRateFor(BigDecimal exchangeRate, String exchangeRateId);

    Observable<ExchangeRateModel> findByBaseCurrencyAndEnabled(String currency, boolean enabled);
}
