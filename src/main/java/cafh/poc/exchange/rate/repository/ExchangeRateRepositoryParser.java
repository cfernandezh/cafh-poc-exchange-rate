package cafh.poc.exchange.rate.repository;

import cafh.poc.exchange.rate.business.domain.ExchangeRateDomain;
import cafh.poc.exchange.rate.repository.Entity.ExchangeRateModel;
import org.springframework.stereotype.Component;

@Component
public class ExchangeRateRepositoryParser {

    public ExchangeRateDomain convertToDomain(ExchangeRateModel model) {
        return  ExchangeRateDomain.builder()
                .exchangeRateId(model.getExchangeRateId())
                .exchangeRate(model.getExchangeRate())
                .baseCurrency(model.getBaseCurrency())
                .currency(model.getCurrency())
                .build();
    }

}
