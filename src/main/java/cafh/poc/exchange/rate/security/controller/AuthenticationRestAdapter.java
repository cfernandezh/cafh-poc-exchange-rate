package cafh.poc.exchange.rate.security.controller;

import cafh.poc.exchange.rate.security.config.UserService;
import cafh.poc.exchange.rate.security.config.JWTUtil;
import io.reactivex.Single;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authentication")
public class AuthenticationRestAdapter {

    private final JWTUtil jwtUtil;

    private final PasswordEncoder passwordEncoder;

    private final UserService userService;

    public AuthenticationRestAdapter(JWTUtil jwtUtil, PasswordEncoder passwordEncoder, UserService userService) {
        this.jwtUtil = jwtUtil;
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    @PostMapping("/login")
    public Single<ResponseEntity<AuthResponse>> login(@RequestBody AuthRequest request) {
        return userService.findByUsername(request.getUsername())
                .filter(userDetails -> passwordEncoder.matches(request.getPassword(), userDetails.getPassword()))
                .map(userDetails -> ResponseEntity.ok(new AuthResponse(jwtUtil.generateToken(userDetails))))
                .switchIfEmpty(Single.just(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()));
    }

}
