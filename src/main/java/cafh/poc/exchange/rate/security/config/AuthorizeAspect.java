package cafh.poc.exchange.rate.security.config;

import cafh.poc.exchange.rate.exception.GlobalErrorInfoEnum;
import cafh.poc.exchange.rate.exception.GlobalErrorInfoException;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Aspect
@Component
public class AuthorizeAspect {

    private final JWTUtil jwtUtil;

    public AuthorizeAspect(JWTUtil jwtUtil) {
        this.jwtUtil = jwtUtil;
    }

    private final String PREFIX = "Bearer ";

    @Around("@annotation(Authorize)")
    public Object authorize(final ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        Authorize authorizeAnnotation = method.getAnnotation(Authorize.class);
        String[] allowedRoles = authorizeAnnotation.value();
        Object[] args = joinPoint.getArgs();
        String tokenParam = String.valueOf(Arrays.stream(args)
                .filter(arg -> arg instanceof String)
                .filter(arg -> ((String) arg).contains(PREFIX))
                .findFirst().get());
        log.info("tokenParam: {}", tokenParam);
        String tokenJwt = tokenParam.replace(PREFIX, "");
        if (tokenJwt == null || !jwtUtil.validateToken(tokenJwt)) {

            throw new GlobalErrorInfoException(GlobalErrorInfoEnum.UNAUTHORIZED);
        }
        Claims claims = jwtUtil.getAllClaimsFromToken(tokenJwt);
        List<String> rolesInToken = claims.get("role", List.class);
        List<String> allowedRolList = Arrays.asList(allowedRoles);
        Set<String> intersect = new HashSet<>(allowedRolList);
        log.info("rolesInToken: {}", rolesInToken);
        log.info("allowedRolList: {}", allowedRolList);
        intersect.retainAll(rolesInToken);
        if (intersect.size()==0) {
            throw new GlobalErrorInfoException(GlobalErrorInfoEnum.FORBIDDEN);
        }
        return joinPoint.proceed();
    }

}
