package cafh.poc.exchange.rate.security.config;

import io.reactivex.Single;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class UserService {

    private final UserConfigData userConfigData;

    public UserService(UserConfigData userConfigData) {
        this.userConfigData = userConfigData;
    }

    private Map<String, User> data;

    @PostConstruct
    public void init() {
        data = new HashMap<>();
        userConfigData.getUsers().stream().forEach(userData -> {
            log.info("password: {}", userData.getPassword());
            data.put(userData.getUsername(),
                    new User(userData.getUsername(), passwordEncoder().encode(userData.getPassword()), userData.getEnabled(),
                            Arrays.asList(userData.getRoles())));
        });
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    public Single<User> findByUsername(String username) {
        return Single.just(data.get(username));
    }

}
