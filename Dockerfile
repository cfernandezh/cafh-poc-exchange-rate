FROM maven:3.8.4-jdk-11 AS builder
WORKDIR /workspace
RUN mvn clean install && ls -ltr target

FROM adoptopenjdk/openjdk11:alpine-jre
WORKDIR /opt/app
COPY --from=builder /workspace/target/cafh-poc-exchange-rate-0.0.1-SNAPSHOT.jar app.jar

ENTRYPOINT ["java","-jar","app.jar"]